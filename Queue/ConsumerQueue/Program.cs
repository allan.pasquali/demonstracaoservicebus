﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ConsumerQueue.Entities;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace ConsumerQueue
{
    public class Program
    {

        protected Program() { }

        const string QueueName = "demonstracao-servicebus";
        static IQueueClient queueClient;

        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[]  args)
        {
            IServiceProvider services = ServiceProviderBuilder.GetServiceProvider(args);
            IOptions<ServiceBusBE> options = services.GetRequiredService<IOptions<ServiceBusBE>>();

            queueClient = new QueueClient(options.Value.StringConexao, QueueName);

            RegisterOnMessageHandlerAndReceiveMessages();
            Console.WriteLine("Aguardando mensagens...");
            Console.ReadKey();
            await queueClient.CloseAsync();
        }

        static void RegisterOnMessageHandlerAndReceiveMessages()
        {
            MessageHandlerOptions messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };

            queueClient.RegisterMessageHandler(ProcessMessagesAsync, messageHandlerOptions);
        }

        static async Task ProcessMessagesAsync(Message message, CancellationToken token)
        {
            Console.WriteLine($"Recebendo mensagem => Sequencial: {message.SystemProperties.SequenceNumber} Mensagem: {Encoding.UTF8.GetString(message.Body)}");
            await queueClient.CompleteAsync(message.SystemProperties.LockToken);
        }

        static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            ExceptionReceivedContext context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }
    }
}