﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SendMesageQueue.Entities;
using System;
using System.Text;
using System.Threading.Tasks;

namespace SendMesageQueue
{
    public class Program
    {

        protected Program() { }

        const string QueueName = "Demonstracao-ServiceBus";
        static IQueueClient queueClient;

        public static async Task Main(string[] args)
        {
            IServiceProvider services = ServiceProviderBuilder.GetServiceProvider(args);
            IOptions<ServiceBusBE> options = services.GetRequiredService<IOptions<ServiceBusBE>>();

            Console.WriteLine("Digite a quantidade de mensagens que deseja enviar: ");
            int.TryParse(Console.ReadLine(), out int quantidadeMensagens);

            queueClient = new QueueClient(options.Value.StringConexao, QueueName);

            await SendMessagesAsync(quantidadeMensagens);
            await queueClient.CloseAsync();
        }

        static async Task SendMessagesAsync(int quantidadeMensagens)
        {
            try
            {
                for (int i = 0; i < quantidadeMensagens; i++)
                {
                    string mensagemString = $"Mensagem {i}";
                    Message mensagem = new Message(Encoding.UTF8.GetBytes(mensagemString));

                    Console.WriteLine($"Enviando mensagem: {mensagemString}");

                    await queueClient.SendAsync(mensagem);
                }
                Console.WriteLine("Mensagens enviadas com sucesso!!!");
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }
    }
}
