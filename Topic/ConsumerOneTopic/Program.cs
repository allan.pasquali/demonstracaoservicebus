﻿using ConsumerOneTopic.Entities;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Text;
using System.Threading.Tasks;

namespace ConsumerOneTopic
{
    public class Program
    {
        protected Program() { }

        const string TopicName = "Demonstracao-ServiceBus-Topic";
        const string Subscription = "ConsumerOne";
        static SubscriptionClient subscriptionClient;

        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            IServiceProvider services = ServiceProviderBuilder.GetServiceProvider(args);
            IOptions<ServiceBusBE> options = services.GetRequiredService<IOptions<ServiceBusBE>>();

            subscriptionClient = new SubscriptionClient(options.Value.StringConexao, TopicName, Subscription);
            await StartAsync();

            Console.WriteLine("Aguardando mensagens...");
            Console.ReadKey();
        }
        


        public static async Task StartAsync()
        {
            Console.WriteLine("############## INICIANDO CONSUMO MENSAGENS CONSUMIDOR 1 ####################");
            Receive();
            await Task.CompletedTask;
        }

        public static void Receive()
        {
            try
            {
                subscriptionClient.RegisterMessageHandler(
                    async (message, token) =>
                    {
                        string messageJson = Encoding.UTF8.GetString(message.Body);

                        Console.WriteLine($"Mensagem recebida: {messageJson}");

                        await subscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
                    },
                    new MessageHandlerOptions(async args => Console.WriteLine(args.Exception))
                    { MaxConcurrentCalls = 1, AutoComplete = false });
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }
    }
}
