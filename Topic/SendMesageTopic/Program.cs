﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SendMesageTopic.Entities;
using System;
using System.Text;
using System.Threading.Tasks;

namespace SendMesageTopic
{
    public class Program
    {
        protected Program() { }

        const string TopicName = "Demonstracao-ServiceBus-Topic";
        static ITopicClient topicClient;

        public static async Task Main(string[] args)
        {
            IServiceProvider services = ServiceProviderBuilder.GetServiceProvider(args);
            IOptions<ServiceBusBE> options = services.GetRequiredService<IOptions<ServiceBusBE>>();

            Console.WriteLine("Digite a quantidade de mensagens que deseja enviar: ");
            int.TryParse(Console.ReadLine(), out int quantidadeMensagens);

            topicClient = new TopicClient(options.Value.StringConexao, TopicName);

            await SendMessagesAsync(quantidadeMensagens);
            await topicClient.CloseAsync();
        }

        static async Task SendMessagesAsync(int quantidadeMensagens)
        {
            try
            {
                for (int i = 0; i < quantidadeMensagens; i++)
                {
                    string mensagemString = $"Mensagem {i}";
                    Message mensagem = new Message(Encoding.UTF8.GetBytes(mensagemString));

                    Console.WriteLine($"Enviando mensagem: {mensagemString}");

                    await topicClient.SendAsync(mensagem);
                }
                Console.WriteLine("Mensagens enviadas com sucesso!!!");
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine($"{DateTime.Now} :: Exception: {exception.Message}");
            }
        }
    }
}
